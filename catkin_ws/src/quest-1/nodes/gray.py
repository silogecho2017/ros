#!/usr/bin/env python
import cv2
import rospy
from std_msgs.msg import String
from sensor_msgs.msg import CompressedImage

# numpy and scipy
import numpy as np
from scipy.ndimage import filters

def gray(data):
    rospy.init_node('talker', anonymous=True)
    rate = rospy.Rate(10) # 10hz
    # cam = cv2.VideoCapture(0)
    # cv2.namedWindow("test")
    # img_counter = 0
    #  #### direct conversion to CV2 ####
    # np_arr = np.fromstring(cam.read(), np.uint8)
    # image_np = cv2.imdecode(np_arr, cv2.CV_LOAD_IMAGE_COLOR)
    while not rospy.is_shutdown():
        rate.sleep()

        if not ret:
            break
        k = cv2.waitKey(1)

        if k%256 == 27:
            # ESC pressed
            print("Escape hit, closing...")
            break
        elif k%256 == 32:
            # SPACE pressed
            # img_name = "opencv_frame_norma{}.png".format(img_counter)
            # cv2.imwrite(img_name, frame)
            # print("{} written!".format(img_name))
            img_counter += 1

            img_name = "opencv_frame_gray{}.png".format(img_counter)
            gray_image = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            cv2.imshow('image',data.data)
            cv2.waitKey(0)
            #cv2.imwrite(img_name, gray_image)
            print("{} written!".format(img_name))

    cam.release()
    cv2.destroyAllWindows()

if __name__ == '__main__':
    try:
        rospy.Subscriber('/image', CompressedImage,gray)  
    except rospy.ROSInterruptException:
        pass

