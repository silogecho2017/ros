#!/usr/bin/env python

# Python libs
import sys, time
 
# numpy and scipy
import numpy as np
from scipy.ndimage import filters
 
# OpenCV
import cv2
 
# Ros libraries
import roslib
import rospy
 
# Ros Messages
from sensor_msgs.msg import CompressedImage
# We do not use cv_bridge it does not support CompressedImage in python
# from cv_bridge import CvBridge, CvBridgeError
 
VERBOSE=False
 
class image_feature:
 
    def __init__(self):
        '''Initialize ros publisher, ros subscriber'''
        # subscribed Topic
        self.subscriber = rospy.Subscriber("/image",
             CompressedImage, self.callback,  queue_size = 1)
        if VERBOSE :
            print "subscribed to /image"
  
   
    def callback(self, ros_data):
       img_counter = 0
       while True:
        arr = np.array(ros_data)
        img_name = "opencv_gray_{}.png".format(img_counter)
        gray_image = cv2.cvtColor(arr, cv2.COLOR_BGR2GRAY)
        cv2.imshow('image',arr)
        cv2.waitKey(0)
        #cv2.imwrite(img_name, gray_image)
        print("{} written!".format(img_name))
        img_counter += 1

def main(args):
    '''Initializes and cleanup ros node'''
    ic = image_feature()
    rospy.init_node('image_feature', anonymous=True)
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print "Shutting down ROS Image feature detector module"
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main(sys.argv)
