#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Twist
from Tkinter import *

class  whatis:
         
    def __init__(self,window):
        self.window = window # Create a window
       
        self.label = Label(window, text = "Move a Turtle in a direction :")
        self.btnForward= Button(window, text = "Forward", command=lambda: self.moveD("forward")) # Create a button
        self.btnBack = Button(window, text = "backward", command=lambda: self.moveD("back")) # Create a button
        self.btndown = Button(window, text = "down", command=lambda: self.moveD("up")) # Create a button
        self.btnUp = Button(window, text = "Up", command=lambda: self.moveD("down")) # Create a button
        self.btnstop = Button(window, text = "Stop", command=lambda: self.moveD("stop")) # Create a button
        self.label.pack() # Place the label in the window
        self.btnBack.pack() # Place the button in the window
        self.btndown.pack() # Place the button in the window
        self.btnForward.pack() # Place the button in the window
        self.btnUp.pack() # Place the button in the window
        self.btnstop.pack()

    def moveD(self,method):
        
        # Starts a new node
        rospy.init_node('GUI', anonymous=True)
        velocity_publisher = rospy.Publisher('/turtle1/cmd_vel', Twist, queue_size=10)
        vel_msg = Twist()
          
        speed = 2  
        distance = 3

        if method == "forward":
            vel_msg.linear.x = abs(speed)
        elif method == "back":
             vel_msg.linear.x = -abs(speed)
        elif method == "up":
             vel_msg.angular.z = -abs(1)
        elif method == "down":
             vel_msg.angular.z = abs(1)
        elif method == "stop":
            rospy.sleep(5)
             
    
           
        #Since we are moving just in x-axis
        vel_msg.linear.y = 0
        vel_msg.linear.z = 0
        vel_msg.angular.x = 0
        vel_msg.angular.y = 0
   
        
       
        #Setting the current time for distance calculus
        t0 = rospy.Time.now().to_sec()
        current_distance = 0
        current_angle = 0;

        #Loop to move the turtle in an specified distance
        while(current_distance < distance):
            #Publish the velocity
            velocity_publisher.publish(vel_msg)
            #Takes actual time to velocity calculus
            t1=rospy.Time.now().to_sec()
            #Calculates distancePoseStamped
            current_distance= speed*(t1-t0)
            current_angle = speed*(t1-t0)
            
        #After the loop, stops the robot
        vel_msg.linear.x = 0
        vel_msg.angular.z = 0
        #Force the robot to stop
        velocity_publisher.publish(vel_msg)
        # rospy.spin()
        

root = Tk();
mygui = whatis(root);
root.mainloop();
        

