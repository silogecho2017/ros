# CMake generated Testfile for 
# Source directory: /home/silas/Documents/ROS/ros_icog/catkin_ws/src
# Build directory: /home/silas/Documents/ROS/ros_icog/catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(question3)
subdirs(question4)
subdirs(ros_fractals)
subdirs(ros_turtlesim_demo)
subdirs(quest-1)
subdirs(quest-2)
subdirs(quest-5)
