set(_CATKIN_CURRENT_PACKAGE "ros_fractals")
set(ros_fractals_VERSION "0.0.0")
set(ros_fractals_MAINTAINER "silas <silas@todo.todo>")
set(ros_fractals_PACKAGE_FORMAT "2")
set(ros_fractals_BUILD_DEPENDS "geometry_msgs" "rospy")
set(ros_fractals_BUILD_EXPORT_DEPENDS "geometry_msgs" "rospy")
set(ros_fractals_BUILDTOOL_DEPENDS "catkin")
set(ros_fractals_BUILDTOOL_EXPORT_DEPENDS )
set(ros_fractals_EXEC_DEPENDS "geometry_msgs" "rospy")
set(ros_fractals_RUN_DEPENDS "geometry_msgs" "rospy")
set(ros_fractals_TEST_DEPENDS )
set(ros_fractals_DOC_DEPENDS )
set(ros_fractals_URL_WEBSITE "")
set(ros_fractals_URL_BUGTRACKER "")
set(ros_fractals_URL_REPOSITORY "")
set(ros_fractals_DEPRECATED "")