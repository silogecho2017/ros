#!/usr/bin/env python
import roslib; roslib.load_manifest('ros_fractals')
import rospy
from geometry_msgs.msg  import Twist
from turtlesim.msg import Pose
from std_srvs.srv import Empty
from turtlesim.srv import Spawn, Kill
from math import pow,atan2,sqrt
import csv

# Star coordinates
starpoints = ((4,2),(6.25,3.25),(9,2),(8.25,4.5),(10,6.25),(7.5,6.5),(6.25,9),(5.5,6.5),(3,6.25),(4.75,4.5),(4,2))
starpoint2 = ((5,3),(6.25,4),(8,3),(7.25,4.5),(8.5,5.75),(7.25,5.75),(6.25,7.25),(5.75,5.75),(4,5.65),(5.5,4.5),(5,3))
starpoint3 = ((5.35,3.65),(6.25,4.25),(7.5,3.65),(7,4.5),(7.95,5.5),(7,5.5),(6.40,6.5),(6.25,5.5),(5,5.5),(5.80,4.5),(5.35,3.65))

# Function for obtaining the coordinates of a custom figure from a csv file
def getpoints(filename):
    reader = csv.DictReader(open(filename), delimiter=',')
    points = [] # The points' coordinates will be saved in a list of tuples
    for row in reader:
        points.append((ord(row['X']) - 48,ord(row['Y']) - 48)) # Each row has one point, whose coordinates are in a 'X' and a 'Y' column
    return points


# Class from which the turtle object will be instanced
class turtlebot():

	# Initialization funcion
    def __init__(self, ini_point, turtlen): # The init funcion receives the initial point for positioning the turtle
        
        while True:
            try: # Try to create a new turtle with the name stored in turtlen
                rospy.wait_for_service('spawn')
                spawner = rospy.ServiceProxy('spawn', Spawn)
                spawner(ini_point[0],ini_point[1], 0, turtlen)
                break
            except rospy.service.ServiceException: # If there already exists a turtle with that name, kill it
                rospy.wait_for_service('kill')
                killer = rospy.ServiceProxy('kill', Kill)
                killer(turtlen)

        # Create the velocity publisher
        self.velocity_publisher = rospy.Publisher(turtlen + '/cmd_vel', Twist, queue_size=10)

        # Create the position subscriber
        self.pose_subscriber = rospy.Subscriber(turtlen + '/pose', Pose, self.callback)
        self.pose = Pose()
        self.rate = rospy.Rate(10) # Refresh frequency [Hz]

        # Create the goal position vector atribute
        self.goal_pose = Pose()

        # Create the velocity vector atribute
        self.vel_msg = Twist()

        # Linear velocity only in the x-axis
        self.vel_msg.linear.y = 0
        self.vel_msg.linear.z = 0

        # Angular velocity only in the z-axis
        self.vel_msg.angular.x = 0
        self.vel_msg.angular.y = 0

    # Callback function formatting the pose value received
    def callback(self, data):
        self.pose = data
        self.pose.x = round(self.pose.x, 4)
        self.pose.y = round(self.pose.y, 4)

    # Calculate the linear (euclidean) distance to the goal point
    def get_linear_distance(self, goal_x, goal_y):
        distance = sqrt(pow((goal_x - self.pose.x), 2) + pow((goal_y - self.pose.y), 2))
        return distance

    # Calculate the angular distance to the goal point
    def get_angular_distance(self, goal_x, goal_y):
        distance = atan2(goal_y - self.pose.y, goal_x - self.pose.x) - self.pose.theta
        return distance

    # Move the turtle to the specified point 
    def move(self, point): # Receives a tuple with the point coordinates

    	# Save the goal point coordinates
        self.goal_pose.x = point[0]
        self.goal_pose.y = point[1]

        # The turtle will first rotate => linear velocity = 0
        self.vel_msg.linear.x = 0

        # The loop will remain until the angular distance starts to increase again
        prev_angular_distance = 100
        angular_distance = self.get_angular_distance(self.goal_pose.x, self.goal_pose.y)
        while abs(angular_distance) <= abs(prev_angular_distance) and abs(angular_distance) > 0.001:

            # Porportional Controller
            self.vel_msg.angular.z = 2 * angular_distance

            # Publish the velocity
            self.velocity_publisher.publish(self.vel_msg)
            self.rate.sleep()

            # Re-calculate the distance
            prev_angular_distance = angular_distance
            angular_distance = self.get_angular_distance(self.goal_pose.x, self.goal_pose.y)


        # The turtle will now translate => angular velocity = 0
        self.vel_msg.angular.z = 0

        # The loop will remain until the linear distance starts to increase again
        prev_linear_distance = 100
        linear_distance = self.get_linear_distance(self.goal_pose.x, self.goal_pose.y)
        while linear_distance <= prev_linear_distance and linear_distance != 0:

            # Porportional Controller
            self.vel_msg.linear.x = 5 * linear_distance 

            # Publish the velocity
            self.velocity_publisher.publish(self.vel_msg)
            self.rate.sleep()

            # Re-calculate the distance
            prev_linear_distance = linear_distance
            linear_distance = self.get_linear_distance(self.goal_pose.x, self.goal_pose.y)
        
        # Stop turtle
        self.vel_msg.linear.x = 0
        self.vel_msg.angular.z =0
        self.velocity_publisher.publish(self.vel_msg)


if __name__ == '__main__':
    try:
        # Initialize node
        rospy.init_node('turtlebot_figure', anonymous=True)

        # Clear the screen
        rospy.wait_for_service('clear')
        clear = rospy.ServiceProxy('clear', Empty)
        clear()

        # The turtle can either draw a star (with pre-specified coordinates) or a custom figure (with coordinates specified in a csv file)
        points = starpoints # Use the pre-specified star coordinates
        point2 = starpoint2
        point3 = starpoint3

        # The figure can be drawn by one or two turtles
        n_turtles = 1
        # reversed_points = points[-1:len(points)/2-1:-1] # the second turtle will go from the end and backwards
        # points = points[0:len(points)/2+1] # and the first turtle will go from the beggining and forwards
        # Instance a turtle objet named turtle1
        x = turtlebot(points[0],'turtle1')

        # Move turtle1 to each point
        for p in points:
            x.move(p)

        # if (n_turtles == 2):
        #     y = turtlebot(reversed_points[0],'turtle2') # Instance a second turtle objet named turtle2
        #     for p in reversed_points:
        #         y.move(p) # Move turtle2 to each point


        # point2 = point2[0:len(point2)/2+1]
        # reversed_point2 = point2[-1:len(point2)/2-1:-1]
        x = turtlebot(point2[0],'turtle1')
        
        # Move turtle1 to each point
        for p in point2:
            x.move(p)

        if (n_turtles == 2):
            y = turtlebot(reversed_point2[0],'turtle2') # Instance a second turtle objet named turtle2
            for p in reversed_point2:
                y.move(p) # Move turtle2 to each point

        x = turtlebot(point3[0],'turtle1')
         
        # Move turtle1 to each point
        for p in point3:
            x.move(p)

        if (n_turtles == 2):
            y = turtlebot(reversed_point2[0],'turtle2') # Instance a second turtle objet named turtle2
            for p in reversed_point2:
                y.move(p) # Move turtle2 to each point


    except rospy.ROSInterruptException: pass
