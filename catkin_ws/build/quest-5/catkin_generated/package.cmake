set(_CATKIN_CURRENT_PACKAGE "quest-5")
set(quest-5_VERSION "0.0.0")
set(quest-5_MAINTAINER "silas <silas@todo.todo>")
set(quest-5_PACKAGE_FORMAT "2")
set(quest-5_BUILD_DEPENDS "roscpp" "rospy" "tf" "turtlesim")
set(quest-5_BUILD_EXPORT_DEPENDS "roscpp" "rospy" "tf" "turtlesim")
set(quest-5_BUILDTOOL_DEPENDS "catkin")
set(quest-5_BUILDTOOL_EXPORT_DEPENDS )
set(quest-5_EXEC_DEPENDS "roscpp" "rospy" "tf" "turtlesim")
set(quest-5_RUN_DEPENDS "roscpp" "rospy" "tf" "turtlesim")
set(quest-5_TEST_DEPENDS )
set(quest-5_DOC_DEPENDS )
set(quest-5_URL_WEBSITE "")
set(quest-5_URL_BUGTRACKER "")
set(quest-5_URL_REPOSITORY "")
set(quest-5_DEPRECATED "")